require 'elasticsearch'
require 'date'

def do_query(event, days)
  client = Elasticsearch::Client.new url: "http://boundless:Boundle$$33@elastic.jawaker.com"

  q = {
    query: {
      query_string: {query: event}
    },
    aggs: {
      by_interval: {
        date_histogram: {
          field: "@timestamp",
          interval: "day"
        },
        aggs: {
          by_interval2: {
            date_histogram: {
              field: "@timestamp",
              interval: "hour"
            }
          }
        }
      }
    }
  }

  client.search index: days, size: 0, body: q
end


def prepare_day(day)
  hours_counter = Time.parse("00:00:00")
  prepared_day = {}
  24.times do
    hours_counter += 3600
    time_as_string = hours_counter.strftime("%H:%M:%S")
    prepared_day[time_as_string] = day[time_as_string] || 0
  end
  prepared_day
end


def prepare_days(dates, buckets)
  days = buckets['aggregations']['by_interval']['buckets']
  prepared_days = []

  result = {}
  if days.size == 0
    prepared_days[0] = prepared_days[1] = prepare_day(result)
  else
    (days.size).times do |d|
      buckets['aggregations']['by_interval']['buckets'][d]['by_interval2']['buckets'].each do |hour|
        time = Time.parse(hour['key_as_string']).strftime("%H:%M:%S")
        result[time] = hour['doc_count']
      end
      prepared_days << prepare_day(result)
    end
  end
  prepared_days << prepare_day({}) if days.size == 1
  prepared_days
end


def get_diff(a, b)
  hours_counter = Time.parse("00:00:00")
  result = {}
  24.times do
    hours_counter += 3600
    time_as_string = hours_counter.strftime("%H:%M:%S")
    result[time_as_string] = (a[time_as_string] - b[time_as_string])**2
  end
  result
end


def get_avg_of_week(event, day)
  client = Elasticsearch::Client.new url: "http://boundless:Boundle$$33@elastic.jawaker.com"
  indices = client.indices.get_aliases.keys.sort[4..-1]
  date_index = indices.find_index(day)
  week_days = indices[date_index - 7, 7]

  buckets = do_query(event, week_days)
  week = prepare_days(week_days, buckets)

  week_sum = []
  week.each_with_index do |d, i|
    week_sum[i] = d.values.inject{ |sum, y| sum + y }
  end
  week_sum.inject{ |sum, y| sum + y } / week_sum.size
end

def get_all_events
  client = Elasticsearch::Client.new url: "http://boundless:Boundle$$33@elastic.jawaker.com"
  indices = client.indices.get_aliases.keys.sort
  buckets = client.search index: indices[indices.size-30,30], size: 0, body: {query: {match_all: {}}, aggs: {events: {terms: {field: 'event', size: 0}}}}
  events = buckets['aggregations']['events']['buckets'].map { |event| event['key'] }
  events
end


def detect_error
  client = Elasticsearch::Client.new url: "http://boundless:Boundle$$33@elastic.jawaker.com"
  indices = client.indices.get_aliases.keys.sort[4..-1]

  i = indices[indices.size-3, 2]
  all_events = get_all_events
  all_events.each do |event|
    buckets = do_query("event: \"#{event}\"", i)
    days = prepare_days(i, buckets)

    d1 = days[0].sort_by { |k, v| Time.parse(k) }.map(&:last).inject(:+)
    d2 = days[1].sort_by { |k, v| Time.parse(k) }.map(&:last).inject(:+)
    per_diff = (d2 - d1).abs / (d1 == 0 ? 0.00001 : d1).to_f * 100
    p event
    p d1
    p d2
    p per_diff
    p "-----------------------"
    puts "There is an error in #{event} event" if (per_diff > 50.0 && d2-d1 > 50)
  end
end
